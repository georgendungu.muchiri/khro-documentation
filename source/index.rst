

.. image:: moh_logo.png
	:align: center



Welcome to KHRO's documentation!
================================

The integrated Kenya Health and Research Observatory is a centralized source of data and research
curated to inform on policy and decision making in the health sector.
KHRO should supports sharing of research and knowledge products between counties, national
government, research institutions and universities.
KHRO is an integrated platform comprising of two interrelated functions:

*  Health Data and Statistics Platform
*  Knowledge Translation Platform (KTP)

Key functions to be performed by KHRO:
======================================

* Monitoring of health situation and trends in public health service delivery 
* Monitoring of progress towards attainment of UHC and SDG
* Monitoring, review, and evaluation of national and disease program-specific strategies and
plans
* Policy analyses: platform to assist translating research evidence into policy
* Harmonize indicators, their baseline and target values, and data sources 
* Harmonize taxonomy for classifying health research using ICD 11 as the baseline. 

Contents:
=========

.. toctree::
   :maxdepth: 1
   
   Architecture
   API
   Developement Standards 
   Deployment
   DHIS2 ETL 
   KHRO DCT Overview



Links:
======

* **Project Management**
	* `Documentation <https://gitlab.com/khro/api_documentation>`_

* **Development**
	* `GitLab <https://gitlab.com/khro>`_
