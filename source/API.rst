API
===

Organization Units
******************

.. image:: organization_units.PNG
	:align: center

Indicators
**********
.. image:: indicators.PNG
	:align: center

Indicator Facts
***************
.. image:: indicator_facts.PNG
	:align: center

Indicator Facts Reported Statistics
***********************************
.. image:: indicator_reported_stats.PNG
	:align: center

Indicator Filter Period Types
*****************************
.. image:: indicator_filter.PNG
	:align: center


Indicator Filter Periods
************************
.. image:: indicator_filter_period.PNG
	:align: center



Indicator Filter Disaggregations
********************************
.. image:: indicator_filter_dissagregations.PNG
	:align: center



Time Periods
************
.. image:: time_periods.PNG
	:align: center


Knowledge Pipelines
*******************
.. image:: knowledge_pipelines.PNG
	:align: center

Commodities
***********
.. image:: commodities.PNG
	:align: center


 Indicator Groups
*****************
.. image:: indicator_groups.PNG
	:align: center


Facility Listings
*****************
.. image:: facility_listings.PNG
	:align: center


Facility Listings Statistics
****************************
.. image:: facility_listings_stats.PNG
	:align: center


Extracting data from KHIS to KHRO Microservice
**********************************************
.. image:: extracting_data.PNG
	:align: center

